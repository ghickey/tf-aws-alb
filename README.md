# Terraform AWS ALB Controller

Deploy the AWS ALB Controller chart in to a Kubernetes cluster.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=0.12 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_chart_name"></a> [chart\_name](#input\_chart\_name) | Chart to install | `string` | `"eks/aws-load-balancer-controller"` | no |
| <a name="input_chart_version"></a> [chart\_version](#input\_chart\_version) | Chart version to install | `string` | `"1.4.1"` | no |
| <a name="input_cluster_state_file"></a> [cluster\_state\_file](#input\_cluster\_state\_file) | Location of the cluster TF state file | `string` | `""` | no |
| <a name="input_department"></a> [department](#input\_department) | Department ID to which this resource belongs | `any` | n/a | yes |
| <a name="input_env_repo"></a> [env\_repo](#input\_env\_repo) | The Terragrunt or environment repo that this module was called from. | `any` | n/a | yes |
| <a name="input_environment_name"></a> [environment\_name](#input\_environment\_name) | The environment (dev, test, prod) | `any` | n/a | yes |
| <a name="input_kube_config_path"></a> [kube\_config\_path](#input\_kube\_config\_path) | Path to kube config for authenticating to Kubernetes | `string` | `"~/.kube/config"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | The namespace to deploy ALB controller into. | `string` | `"kube-system"` | no |
| <a name="input_nugget"></a> [nugget](#input\_nugget) | Naming moniker for relating all deployed objects | `any` | `null` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | The overall product that will be deployed in this infrastructure | `any` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | Google project ID | `any` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The region to deploy to (e.g. us-east-1) | `any` | n/a | yes |
| <a name="input_release_name"></a> [release\_name](#input\_release\_name) | The release name that Helm knows this resource by | `string` | `"alb-controller"` | no |
| <a name="input_state_bucket"></a> [state\_bucket](#input\_state\_bucket) | The bucket name for terraform state | `any` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_chart_metadata"></a> [chart\_metadata](#output\_chart\_metadata) | Metadata collected from chart deployment |
