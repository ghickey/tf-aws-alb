
variable "namespace" {
  description = "The namespace to deploy ALB controller into."
  type        = string
  default     = "kube-system"
}

variable "release_name" {
  description = "The release name that Helm knows this resource by"
  type        = string
  default     = "alb-controller"
}

variable "chart_name" {
  description = "Chart to install"
  type        = string
  default     = "eks/aws-load-balancer-controller"
}

variable "chart_version" {
  description = "Chart version to install"
  type        = string
  default     = "1.4.1"
}

variable "cluster_state_file" {
  description = "Location of the cluster TF state file"
  type        = string
  default     = ""
}

variable "kube_config_path" {
  default     = "~/.kube/config"
  description = "Path to kube config for authenticating to Kubernetes"
}
