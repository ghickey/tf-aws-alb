
terraform {
  required_version = ">=0.12"

  backend "http" {}
}

data "terraform_remote_state" "cluster" {
  backend = "http"
  config = {
    address = var.cluster_state_file
  }
}

provider "helm" {
  kubernetes {
    config_path = var.kube_config_path
  }
}

resource "null_resource" "helm_repo_setup" {
  provisioner "local-exec" {
    command = "helm repo add eks https://aws.github.io/eks-charts"
  }
}

module "aws-iam" {
  count = 0

  source = "git::ssh://git@gitlab.com/ghickey/tf-aws-iam.git?ref=v1.0.0"

  name = "${data.terraform_remote_state.cluster.outputs.name}-alb-controller"
  # https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.4.1/docs/install/iam_policy.json
  principals = [
    {
      name = "CreateServiceLink"
      list = [ "iam:CreateServiceLinkedRole" ]
      resources = [ "*" ]
    },
    {
      name = "EC2Access"
      list = [ "ec2:DescribeAccountAttributes",
               "ec2:DescribeAddresses",
               "ec2:DescribeAvailabilityZones",
               "ec2:DescribeInternetGateways",
               "ec2:DescribeVpcs",
               "ec2:DescribeVpcPeeringConnections",
               "ec2:DescribeSubnets",
               "ec2:DescribeSecurityGroups",
               "ec2:DescribeInstances",
               "ec2:DescribeNetworkInterfaces",
               "ec2:DescribeTags",
               "ec2:GetCoipPoolUsage",
               "ec2:DescribeCoipPools",
               "elasticloadbalancing:DescribeLoadBalancers",
               "elasticloadbalancing:DescribeLoadBalancerAttributes",
               "elasticloadbalancing:DescribeListeners",
               "elasticloadbalancing:DescribeListenerCertificates",
               "elasticloadbalancing:DescribeSSLPolicies",
               "elasticloadbalancing:DescribeRules",
               "elasticloadbalancing:DescribeTargetGroups",
               "elasticloadbalancing:DescribeTargetGroupAttributes",
               "elasticloadbalancing:DescribeTargetHealth",
               "elasticloadbalancing:DescribeTags" ]
      resources = [ "*" ]
    },
    {
      name = "WAFAccess"
      list = [ "cognito-idp:DescribeUserPoolClient",
               "acm:ListCertificates",
               "acm:DescribeCertificate",
               "iam:ListServerCertificates",
               "iam:GetServerCertificate",
               "waf-regional:GetWebACL",
               "waf-regional:GetWebACLForResource",
               "waf-regional:AssociateWebACL",
               "waf-regional:DisassociateWebACL",
               "wafv2:GetWebACL",
               "wafv2:GetWebACLForResource",
               "wafv2:AssociateWebACL",
               "wafv2:DisassociateWebACL",
               "shield:GetSubscriptionState",
               "shield:DescribeProtection",
               "shield:CreateProtection",
               "shield:DeleteProtection" ]
      resources = [ "*" ]
    },
    {
      name = "EC2SecurityGroupIngress"
      list = [ "ec2:AuthorizeSecurityGroupIngress",
               "ec2:RevokeSecurityGroupIngress" ]
      resources = [ "*" ]
    },
    {
      name = "EC2CreateSecurityGroup"
      list = [ "ec2:CreateSecurityGroup",
               "ec2:DeleteSecurityGroup" ]
      resources = [ "*" ]
    },
    {
      name = "EC2Tags"
      list = [ "ec2:CreateTags",
               "ec2:DeleteTags" ]
      resources = [ "arn:aws:ec2:*:*:security-group/*",
                    "arn:aws:ec2:*:*:network-interface/*" ]
    },
    {
      name = "LoadBalancerGroup"
      list = [ "elasticloadbalancing:CreateLoadBalancer",
               "elasticloadbalancing:CreateTargetGroup",
               "elasticloadbalancing:CreateListener",
               "elasticloadbalancing:DeleteListener",
               "elasticloadbalancing:CreateRule",
               "elasticloadbalancing:DeleteRule" ]
      resources = [ "*" ]
    },
    {
      name = "LoadBalancerTags"
      list = [ "elasticloadbalancing:AddTags",
               "elasticloadbalancing:RemoveTags" ]
      resources = [ "arn:aws:elasticloadbalancing:*:*:targetgroup/*/*",
                    "arn:aws:elasticloadbalancing:*:*:loadbalancer/net/*/*",
                    "arn:aws:elasticloadbalancing:*:*:loadbalancer/app/*/*" ]
    },
    {
      name = "LoadBalancerListenerTags"
      list = [ "elasticloadbalancing:AddTags",
               "elasticloadbalancing:RemoveTags" ]
      resources = [ "arn:aws:elasticloadbalancing:*:*:listener/net/*/*/*",
                    "arn:aws:elasticloadbalancing:*:*:listener/app/*/*/*",
                    "arn:aws:elasticloadbalancing:*:*:listener-rule/net/*/*/*",
                    "arn:aws:elasticloadbalancing:*:*:listener-rule/app/*/*/*" ]
    },
    {
      name = "LoadBalancer"
      list = [ "elasticloadbalancing:ModifyLoadBalancerAttributes",
               "elasticloadbalancing:SetIpAddressType",
               "elasticloadbalancing:SetSecurityGroups",
               "elasticloadbalancing:SetSubnets",
               "elasticloadbalancing:DeleteLoadBalancer",
               "elasticloadbalancing:ModifyTargetGroup",
               "elasticloadbalancing:ModifyTargetGroupAttributes",
               "elasticloadbalancing:DeleteTargetGroup" ]
      resources = [ "*" ]
    },
    {
      name = "LoadBalancerTargets"
      list = [ "elasticloadbalancing:RegisterTargets",
               "elasticloadbalancing:DeregisterTargets" ]
      resources = [ "arn:aws:elasticloadbalancing:*:*:targetgroup/*/*" ]
    },
    {
      name = "LoadBalancerCerts"
      list = [ "elasticloadbalancing:SetWebAcl",
               "elasticloadbalancing:ModifyListener",
               "elasticloadbalancing:AddListenerCertificates",
               "elasticloadbalancing:RemoveListenerCertificates",
               "elasticloadbalancing:ModifyRule" ]
      resources = [ "*" ]
    },
    {
      name = "VPC-IPv4"
      list = [ "ec2:AssignPrivateIpAddresses",
               "ec2:AttachNetworkInterface",
               "ec2:CreateNetworkInterface",
               "ec2:DeleteNetworkInterface",
               "ec2:DescribeInstances",
               "ec2:DescribeTags",
               "ec2:DescribeNetworkInterfaces",
               "ec2:DescribeInstanceTypes",
               "ec2:DetachNetworkInterface",
               "ec2:ModifyNetworkInterfaceAttribute",
               "ec2:UnassignPrivateIpAddresses" ]
      resources = [ "*" ]
    },
    {
      name = "VPC-IPv6"
      list = [ "ec2:AssignIpv6Addresses",
               "ec2:DescribeInstances",
               "ec2:DescribeTags",
               "ec2:DescribeNetworkInterfaces",
               "ec2:DescribeInstanceTypes" ]
      resources = [ "*" ]
    }
  ]
}

module "load_balancer_controller_irsa_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name = "aws-load-balancer-controller"
  create_role = true

  attach_load_balancer_controller_policy = true
  attach_vpc_cni_policy                  = true

  oidc_providers = {
    ex = {
      provider_arn               = data.terraform_remote_state.cluster.outputs.oidc_provider_arn
      namespace_service_accounts = ["kube-system:aws-load-balancer-controller"]
    }
  }
}

resource "helm_release" "alb-controller" {
  name         = var.release_name
  chart        = var.chart_name
  version      = var.chart_version
  namespace    = var.namespace
  force_update = true

  values = [ <<EOF
clusterName: ${data.terraform_remote_state.cluster.outputs.name}
serviceAccount:
  create: true
  name: aws-load-balancer-controller
  annotations:
    eks.amazonaws.com/role-arn: ${module.load_balancer_controller_irsa_role.iam_role_arn}
podAnnotations:
  eks.amazonaws.com/role-arn: ${module.load_balancer_controller_irsa_role.iam_role_arn}
EOF
]

}
