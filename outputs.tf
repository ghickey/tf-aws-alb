
# output "service_account_name" {
#   description = "Name of the service account created for external-dns"
#   value       = google_service_account.external_dns.name
# }

# output "service_account_id" {
#   description = "ID of the created service account"
#   value       = google_service_account.external_dns.id
# }

# output "service_account_email" {
#   description = "Email address of the created service account"
#   value       = google_service_account.external_dns.email
# }

output "chart_metadata" {
  description = "Metadata collected from chart deployment"
  value       = helm_release.alb-controller.metadata
}

output "iam_role_arn" {
  value = module.load_balancer_controller_irsa_role.iam_role_arn
}

output "iam_role_name" {
  value = module.load_balancer_controller_irsa_role.iam_role_name
}

output "iam_role_path" {
  value = module.load_balancer_controller_irsa_role.iam_role_path
}

output "iam_role_unique_id" {
  value = module.load_balancer_controller_irsa_role.iam_role_unique_id
}

